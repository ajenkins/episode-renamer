#!/usr/bin/env python2
import os
import os.path
import re
import argparse

class Renamer(object):
    def __init__(self, show, season, pattern, directory):
        self.show = show
        self.season = season
        self.pattern = pattern
        self.directory = directory
        self.rename()

    def rename(self):
        epre = re.compile(self.pattern)
        for dirpath, dirnames, filenames in os.walk(self.directory):
            for filename in filenames:
                match = epre.search(filename)
                if match:
                    fileext = filename[filename.rfind('.'):]
                    new_name = "%s - s%ie%i%s" % (self.show, self.season, int(match.group(1)), fileext)
                    oldpath = os.path.join(dirpath, filename)
                    newpath = os.path.join(dirpath, new_name)
                    print("Renaming from %s to %s" % (oldpath, newpath))
                    os.rename(oldpath, newpath)
                else:
                    print("%s did not match regex." % filename)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Rename a group of episodes of a TV show to allow Sickbeard to rename them.")

    # show name
    parser.add_argument('show', metavar='show', type=str, help='The name of the show the episodes belong to.')

    # season
    parser.add_argument('season', metavar='season', type=int, help='The number of the season that the episodes in the directory belong to.')

    # pattern
    parser.add_argument('pattern', metavar='pattern', type=str, help='A regular expression pattern that shows where the episodes number is in the existing name.')

    # directory
    parser.add_argument('directory', metavar='directory', type=str, help='The path of the directory to process')

    args = parser.parse_args()

    renamer = Renamer(args.show, args.season, args.pattern, args.directory)

